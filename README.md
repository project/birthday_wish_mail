Birthday Wish Mail
---------------------------

INTRODUCTION
-----------
  Send automatically birthday wish mail to the user. 
  When the user's birthday comes.

INSTALLATION:
-------------
  1. Install as you would normally install a contributed Drupal module. 
     See: https://www.drupal.org/node/895232 for further information.

REQUIREMENTS
------------
  The basic birthday wish mail module has token module dependencies.

CONFIGURATION
-------------
  1. Install module "Birthday Wish Mail".
  2. Admin config Url:- admin/config/birthday_wish_mail
  3. Set machine name from user manage fields in "DOB" field . 
     Eg:- "field_date_of_birth".
  4. Add mail "Subject" and "Body".
  5. Click on Save configuration button.

UNINSTALLATION
--------------
  1. Uninstall as you would normally Uninstall a contributed Drupal module. 
     See: https://www.drupal.org/docs/user_guide/en/config-uninstall.html 
     for further information.
